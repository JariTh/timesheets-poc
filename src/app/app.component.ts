import { Component, ViewChild, Output, EventEmitter } from "@angular/core";
import { IonInput, ModalController } from "@ionic/angular";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent {
  columns = [{ name: "Persoon", prop: "persoon" }];
  registratieFormulier = {
    persoon: "",
    project: "",
    activiteit: "",
    looncode: "",
    duur: "",
    commentaar: ""
  };
  registraties = [];
  isMobile = false;
  showGrid = false;
  @ViewChild("persoonInput") inputElement: IonInput;

  constructor(private modalController: ModalController) {
    if (
      /Android|webOS|iPhone|iPad|iPod|Opera Mini/i.test(navigator.userAgent)
    ) {
      this.isMobile = true;
    }
  }

  save() {
    this.registraties.unshift(this.registratieFormulier);
    this.registraties = [...this.registraties];
    this.registratieFormulier = {
      persoon: "",
      project: "",
      activiteit: "",
      looncode: "",
      duur: "",
      commentaar: ""
    };
    if (this.inputElement) {
      this.inputElement.setFocus();
    }
  }

  toggleGrid() {
    this.showGrid = !this.showGrid;
  }

  showModal() {
    const modal = this.modalController.create({
      component: ModalPageComponent
    });
    let data;

    modal
      .then(result => {
        result.present();
        return modal;
      })
      .then(result => {
        return result.onWillDismiss();
      })
      .then(result => {
        if (!result.data.registratie) {
          return;
        }
        this.registratieFormulier = result.data.registratie;
        this.save();
      });
    console.log(data);
  }
}

@Component({
  selector: "modal-page",
  template: `
    <ion-header>
      <ion-toolbar>
        <ion-title>Toevoegen</ion-title>
        <ion-buttons slot="end">
          <ion-button (click)="dismissModal()">Close</ion-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
    <ion-content>
      <ion-item>
        <ion-label position="floating">Persoon</ion-label>
        <ion-input
          #persoonInput
          [(ngModel)]="registratieFormulier.persoon"
        ></ion-input>
      </ion-item>

      <ion-item>
        <ion-label position="floating">Project</ion-label>
        <ion-input [(ngModel)]="registratieFormulier.project"></ion-input>
      </ion-item>

      <ion-item>
        <ion-label position="floating">Activiteit</ion-label>
        <ion-input [(ngModel)]="registratieFormulier.activiteit"></ion-input>
      </ion-item>

      <ion-item>
        <ion-label position="floating">Looncode</ion-label>
        <ion-input [(ngModel)]="registratieFormulier.looncode"></ion-input>
      </ion-item>

      <ion-item>
        <ion-label position="floating">Duur</ion-label>
        <ion-input [(ngModel)]="registratieFormulier.duur"></ion-input>
      </ion-item>

      <ion-item>
        <ion-label position="floating">Commentaar</ion-label>
        <ion-input [(ngModel)]="registratieFormulier.commentaar"></ion-input>
      </ion-item>

      <ion-button (click)="onSave()">Opslaan</ion-button>
    </ion-content>
  `
})
export class ModalPageComponent {
  registratieFormulier = {
    persoon: "",
    project: "",
    activiteit: "",
    looncode: "",
    duur: "",
    commentaar: ""
  };
  @Output() save = new EventEmitter<any>();

  constructor(private modalCtrl: ModalController) {}

  dismissModal() {
    this.modalCtrl.dismiss({
      dismissed: true
    });
  }

  onSave() {
    this.save.emit(this.registratieFormulier);
    this.modalCtrl.dismiss({
      dismissed: true,
      registratie: this.registratieFormulier
    });
  }
}
